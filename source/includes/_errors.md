# Errors

Error Code | Meaning
---------- | -------
400 | Bad Request -- Your request is invalid.
401 | Unauthorized -- Your API key is wrong.
402 | Payment Required -- Something is wrong with your payment.
420 | Chill out.
500 | Internal Server Error -- We had a problem with our server. Try again later.
