---
title: API Reference

language_tabs: # must be one of https://git.io/vQNgJ
  - shell

toc_footers:
  - <a href='#'>Sign Up for a Developer Key</a>

includes:
  - errors

search: true
---

# Introduction

Welcome to the PhonePrivacy API Documentation. PhonePrivacy API will allow you programmatic access to your phone.

Keep note that despite PhonePrivacy being limited to US-only for the time being, including the country code (+1) is still required.

# Authentication

> To authorize:

```shell
# With shell, you can just pass the correct header with each request
curl "api_endpoint_here"
  -H "Authorization: API [API KEY]"
```

<aside class="notice">
You must replace <code>[API Key]</code> with your personal API key.
</aside>

# User

## Get

Retrieve the user document

The user document will contain all of the user's numbers and other profile information.

```shell
curl "https://phoneprivacy.co/api/v1/user"
  -H "Authorization: API [API KEY]"
```

> The above command returns JSON structured like this:

```json
{
    "_id": "5dc83ccde0abdf1864742586",
    "username": "maddie@qnzl.co",
    "activeSIM": "XXXXXXXXXXXXXXX",
    "profile": {
        "enabledServices": {
            "sms": true,
            "data": true,
            "phone": true
        },
        "subscriptionItems": [
            {
                "id": "si_XXXXXXXXXXXX",
                "nickname": "Data Replace",
            }
        ],
        "customerId": "cus_XXXXXXXXXXX",
        "items": {},
        "subscriptionId": "sub_XXXXXXXXXX"
    },
    "api": {
        "webhooks": {
            "smsIncoming": [],
            "callIncoming": []
        },
        "key": "XXXX-XXXX-XXXX-XXXX"
    },
    "numbers": [
        {
            "number": "+1XXXXXXXXXX",
            "sid": "PNXXXXXXXXXXXXXXXXX",
            "name": "Personal",
            "whitelist": [
                {
                    "number": "+1XXXXXXXXXX",
                    "name": "Bae"
                }
            ],
            "options": {
                "blacklist": false,
                "whitelist": false,
                "forwarding": true,
                "roboCheck": false,
                "pin": ""
            },
            "blacklist": []
        }
    ],
    "forwardedNumbers": [],
}
```

# SMS

## Send

`from` must be a number controlled by your PhonePrivacy account.

<aside class="notice">
`sid` is the Twilio ID, not the previously mentioned ID
</aside>

```shell
curl "https://phoneprivacy.co/api/v1/sms"
  -H "Authorization: API [API KEY]"
  -H "Content-Type: application/json"
  -X POST
  -d '{"from": "+19119119111", "to": "+19129119111", "body": "Hi!"}'
```

## Get SMS Thread(s)

`id` is the MD5 of `[their number]` + `-` + `[your number]`

<aside class="notice">
`id` is optional, if it is not included all threads will be returned.
</aside>

```shell
curl "https://phoneprivacy.co/api/v1/sms/:id?"
  -H "Authorization: API [API KEY]"
```
> The above command returns JSON structured like this:

```json
{
    "threads": {
        "01914cc12296a64eXXXXc8081520f": {
            "expanded": false,
            "from": "+1XXXXXXXXXX",
            "latestMessage": "2019-09-27T20:48:18.000Z",
            "messages": [
                {
                    "body": "We should be there shortly. ",
                    "dateSent": "2019-09-27T20:48:18.000Z",
                    "from": "+1XXXXXXXXXX",
                    "numMedia": "0",
                    "price": "-0.00750",
                    "sid": "SMXXXXXXXXXXXXX",
                    "to": "+1XXXXXXXXXX"
                }
            ],
            "name": "+1XXXXXXXXXX (Personal)",
            "show": 10,
            "to": "+1XXXXXXXXXX"
        },
        "32acbb74191e77dXXXXXXX6b7e469a6": {
            "expanded": false,
            "from": "+1XXXXXXXXXX",
            "latestMessage": "2019-10-05T17:15:52.000Z",
            "messages": [
                {
                    "body": "Hi!",
                    "dateSent": "2019-10-05T17:15:52.000Z",
                    "from": "+1XXXXXXXXXX",
                    "numMedia": "0",
                    "price": "-0.00750",
                    "sid": "SMXXXXXXXXXXXXX",
                    "to": "+1XXXXXXXXXX"
                }
            ],
            "name": "+1XXXXXXXXXX (Personal)",
            "show": 10,
            "to": "+1XXXXXXXXXX"
        }
    },
    "unreads": {}
  }
}

```

## Delete SMS

<aside class="notice">
`sid` is the Twilio ID, not the previously mentioned ID
</aside>

```shell
curl "https://phoneprivacy.co/api/v1/sms/:sid"
  -H "Authorization: API [API KEY]"
  -X DELETE
```

## Rename thread

Renames the thread, only currently visible on PhonePrivacy

<aside class="notice">
`id` is the MD5 of `[their number]` + `-` + `[your number]`
</aside>

```shell
curl "https://phoneprivacy.co/api/v1/sms/:sid/?dump=false"
  -H "Authorization: API [API KEY]"
  -H "Content-Type: application/json"
  -X PUT
  -d '{"name": "new thread name"}'
```

# Phone Numbers

## Get list of available phone numbers

Gets 10 available numbers that match the query

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
areaCode | null | Desired area code of the number
contains | null | Substring of numbers contained within the number

<aside class="info">
`areaCode` or `contains` is required. They are not additive; `areaCode` has precedence over `contains` if both are sent.

</aside>

```shell
curl "https://phoneprivacy.co/api/v1/numbers/available"
  -H "Authorization: API [API KEY]"
```

> The above command returns JSON structured like this:

```json
{
    "numbers": [
        {
            "capabilities": {
                "MMS": true,
                "SMS": true,
                "fax": true,
                "voice": true
            },
            "friendlyName": "(319) 302-3456",
            "notFriendlyName": "+13193023456"
        },
        {
            "capabilities": {
                "MMS": true,
                "SMS": true,
                "fax": true,
                "voice": true
            },
            "friendlyName": "(319) 220-4915",
            "notFriendlyName": "+13192204915"
        },
        ...
    ]
}

```
## Buy

```shell
curl "https://phoneprivacy.co/api/v1/numbers/buy"
  -H "Authorization: API [API KEY]"
  -H "Content-Type: application/json"
  -X PUT
  -d '{"number": "+19119119111", "name": "name is optional"}'
```

## Release / Delete

Releases a number from your account. PhonePrivacy retains access to the number until the end of the month. All released numbers are blocked from purchase.

If the number is opened for sale again (no current plans to unblock list numbers), all history before the purchase is inaccessible to the new user.

<aside class="info">
`index` is the 0-based index of the number contained in `user.numbers`. Yes, it is dumb. Yes, I am sorry. Yes, I am going to change it.
</aside>

```shell
curl "https://phoneprivacy.co/api/v1/numbers/:index"
  -H "Authorization: API [API KEY]"
  -X DELETE
```

## Add to Allow List

Adds a number to the list of numbers that will be forwarded to your number. By adding to the allow list, there is a side effect of enabling the allow list and disabling the block list.

<aside class="info">
`index` is the 0-based index of the number contained in `user.numbers`. Yes, it is dumb. Yes, I am sorry. Yes, I am going to change it.
</aside>

```shell
curl "https://phoneprivacy.co/api/v1/numbers/:index/allowlist"
  -H "Authorization: API [API KEY]"
  -H "Content-Type: application/json"
  -X POST
  -d '{"number": "+19119119111", "name": "name is optional"}'
```

## Remove from Allow List

Removes a number from the list of numbers allowed to be forwarded.

If the removal causes the list to be empty, the allow list is disabled.

<aside class="info">
`index` is the 0-based index of the number contained in `user.numbers`. Yes, it is dumb. Yes, I am sorry. Yes, I am going to change it.
</aside>

```shell
curl "https://phoneprivacy.co/api/v1/numbers/:index/allowlist/:allowListIndex"
  -H "Authorization: API [API KEY]"
  -X DELETE
```

## Add to Block List

Adds a number to the list of numbers that will be blocked. By adding to the allow list, there is a side effect of enabling the block list and disabling the allow list.

<aside class="info">
`index` is the 0-based index of the number contained in `user.numbers`. Yes, it is dumb. Yes, I am sorry. Yes, I am going to change it.
</aside>

```shell
curl "https://phoneprivacy.co/api/v1/numbers/:index/blockList"
  -H "Authorization: API [API KEY]"
  -H "Content-Type: application/json"
  -X POST
  -d '{"number": "+19119119111", "name": "name is optional"}'
```
## Remove from Block List

Removes a number from the list of numbers automatically blocked.

If the removal causes the list to be empty, the block list is disabled.

<aside class="info">
`index` is the 0-based index of the number contained in `user.numbers`. Yes, it is dumb. Yes, I am sorry. Yes, I am going to change it.
</aside>

```shell
curl "https://phoneprivacy.co/api/v1/numbers/:index/blocklist/:blockListIndex"
  -H "Authorization: API [API KEY]"
  -X DELETE
```

## Update Options

### Available Options

Parameter | Description
--------- | -----------
forwarding | If calls to this number should be forwarded to your number
blocklist |  Whether the block list is enabled
allowlist | Whether the allow list is enabled
pin | String of numbers required to be entered before call is forwarded (a password)
roboCheck | Whether callers should be prompted to enter a random digit to prove they're human

<aside class="info">
`index` is the 0-based index of the number contained in `user.numbers`. Yes, it is dumb. Yes, I am sorry. Yes, I am going to change it.
</aside>

```shell
curl "https://phoneprivacy.co/api/v1/numbers/:index/options"
  -H "Authorization: API [API KEY]"
  -X PUT
  -d '{}'
```

# Webhooks

There are currently two webhooks, SMS incoming and call incoming.

Both of these webhooks are called before PhonePrivacy takes action on call, allowing users to shape these incoming actions as desired. Both need to use the POST method.

For the call incoming webhook, the intial request will have `{ verify: true }`. Response code 202 will redirect the call to your URL and all other response codes do nothing.

For the SMS incoming webhook, the initial request will contain the entire message; response codes are not checked.

Webhook URLs can be set on the Accounts page of PhonePrivacy.


